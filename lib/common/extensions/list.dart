extension IsNullOrEmptyEnhancements on List {
  bool get isNullOrEmpty => this?.isEmpty ?? true;
}
