import 'package:binder/binder.dart';
import 'package:meta/meta.dart';
import 'package:weather_app/data/data.dart';

abstract class BaseRepository with Logic {
  const BaseRepository();

  @protected
  @nonVirtual
  MetaWeatherDataSource get metaWeatherDataSource =>
      read(metaWeatherDataSourceRef);
}
