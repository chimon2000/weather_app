import 'package:binder/binder.dart';
import 'package:weather_app/data/data.dart';

import 'base_repository.dart';

final weatherRepositoryRef = LogicRef((scope) => WeatherRepository(scope));

class WeatherRepository extends BaseRepository {
  const WeatherRepository(this.scope);

  @override
  final Scope scope;

  Future<Weather> getWeather(int locationId) async {
    final weather = await metaWeatherDataSource.getWeather(locationId);

    return weather;
  }
}
