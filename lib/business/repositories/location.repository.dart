import 'package:binder/binder.dart';
import 'package:weather_app/data/data.dart';

import 'base_repository.dart';

final locationRepositoryRef = LogicRef((scope) => LocationRepository(scope));

class LocationRepository extends BaseRepository {
  const LocationRepository(this.scope);

  @override
  final Scope scope;

  Future<Location> searchLocation(String query) async {
    final location = await metaWeatherDataSource.searchLocation(query);

    return location;
  }
}
