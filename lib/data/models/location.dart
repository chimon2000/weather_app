import 'package:equatable/equatable.dart';

class Location extends Equatable {
  factory Location.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Location._(
      title: map['title'],
      locationType: map['location_type'],
      id: map['woeid'],
    );
  }

  Location._({
    this.locationType,
    this.id,
    this.title,
  });

  final int id;
  final String title;
  final String locationType;

  @override
  List<Object> get props => [title, locationType, id];
}
