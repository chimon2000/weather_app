import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Weather extends Equatable {
  factory Weather.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Weather._(
      id: map['id'],
      weatherStateName: map['weather_state_name'],
      weatherStateAbbr: map['weather_state_abbr'],
      minTemp: map['min_temp'],
      maxTemp: map['max_temp'],
      theTemp: map['the_temp'],
      windSpeed: map['wind_speed'],
      windDirection: map['wind_direction_compass'],
      airPressure: map['air_pressure'],
      humidity: map['humidity'],
      visibility: map['visibility'],
      predictability: map['predictability'],
      created: DateTime.tryParse(map['created']),
      applicableDate: DateTime.tryParse(map['applicable_date']),
    );
  }

  Weather._({
    @required this.id,
    @required this.weatherStateName,
    @required this.weatherStateAbbr,
    @required this.minTemp,
    @required this.maxTemp,
    @required this.theTemp,
    @required this.windSpeed,
    @required this.windDirection,
    @required this.airPressure,
    @required this.humidity,
    @required this.visibility,
    @required this.predictability,
    @required this.created,
    @required this.applicableDate,
  });

  final int id;
  final String weatherStateName;
  final String weatherStateAbbr;
  final String windDirection;
  final double minTemp;
  final double maxTemp;
  final double theTemp;
  final double windSpeed;
  final double airPressure;
  final int humidity;
  final double visibility;
  final int predictability;
  final DateTime created;
  final DateTime applicableDate;

  @override
  List<Object> get props {
    return [
      id,
      weatherStateName,
      weatherStateAbbr,
      minTemp,
      maxTemp,
      theTemp,
      windSpeed,
      windDirection,
      airPressure,
      humidity,
      visibility,
      predictability,
      created,
      applicableDate,
    ];
  }

  @override
  bool get stringify => true;
}
