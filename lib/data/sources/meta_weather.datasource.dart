import 'package:binder/binder.dart';
import 'package:dio/dio.dart';
import 'package:weather_app/data/models/models.dart';
import 'package:weather_app/common/common.dart';

final metaWeatherDataSourceRef = StateRef(MetaWeatherDataSource());

class MetaWeatherDataSource {
  const MetaWeatherDataSource([this._dio]);

  final Dio _dio;

  Future<Location> searchLocation(String query) async {
    final locationResponse =
        await _dio.get('/location/search', queryParameters: {'query': query});
    if (locationResponse.statusCode != 200) {
      throw LocationSearchRequestFailure();
    }

    final locationJson = locationResponse.data as List;
    return locationJson.isNullOrEmpty
        ? null
        : Location.fromMap(locationJson.first);
  }

  Future<Weather> getWeather(int locationId) async {
    final weatherResponse = await _dio.get('/location/$locationId');

    if (weatherResponse.statusCode != 200) {
      throw WeatherRequestFailure();
    }
    final weatherJson = weatherResponse.data['consolidated_weather'] as List;
    return weatherJson.isNullOrEmpty
        ? null
        : Weather.fromMap(weatherJson.first);
  }
}

class LocationSearchRequestFailure implements Exception {}

class WeatherRequestFailure implements Exception {}
