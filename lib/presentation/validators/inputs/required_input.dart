import 'package:formz/formz.dart';

class RequiredInput extends FormzInput<String, RequiredInputError> {
  const RequiredInput.pure() : super.pure('');
  const RequiredInput.dirty({String value = ''}) : super.dirty(value);

  @override
  RequiredInputError validator(String value) {
    if (value?.isEmpty == true) return RequiredInputError.empty;

    return null;
  }
}

enum RequiredInputError { empty }
