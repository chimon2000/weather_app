import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

import 'package:weather_app/presentation/validators/inputs/inputs.dart';

class HomePageValidator extends Equatable with FormzMixin {
  const HomePageValidator({
    this.locationSearchQuery = const RequiredInput.pure(),
  });

  final RequiredInput locationSearchQuery;
  @override
  List<FormzInput> get inputs => [locationSearchQuery];

  @override
  List<Object> get props => [locationSearchQuery];

  HomePageValidator copyWith({
    RequiredInput locationSearchQuery,
  }) {
    return HomePageValidator(
      locationSearchQuery: locationSearchQuery ?? this.locationSearchQuery,
    );
  }
}
