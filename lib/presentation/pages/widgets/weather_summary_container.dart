import 'package:flutter/material.dart';
import 'package:binder/binder.dart';
import 'package:remote_state/remote_state.dart';
import 'package:weather_app/data/data.dart';
import 'package:weather_app/presentation/logic/home_page.logic.dart';
import 'package:weather_app/presentation/pages/widgets/weather_summary.dart';

class WeatherSummaryContainer extends StatelessWidget {
  const WeatherSummaryContainer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StateListener(
      watchable: homePageStateRef.select((state) => state.location),
      onStateChanged: (context, location) =>
          context.use(homePageLogicRef).getWeather(),
      child: Consumer<RemoteState<Weather>>(
        watchable: homePageStateRef.select((state) => state.weather),
        builder: (context, weather, child) {
          final tempuratureUnit = context
              .watch(homePageStateRef.select((state) => state.tempuratureUnit));
          return weather.when(
            initial: WeatherSummaryContainer.initial,
            loading: WeatherSummaryContainer.placeholder,
            success: (weather) => WeatherSummary(
              weather: weather,
              tempuratureUnit: tempuratureUnit,
            ),
            error: WeatherSummaryContainer.error,
          );
        },
      ),
    );
  }

  static Widget placeholder() => Center(
      key: weatherSummaryPlaceholderKey, child: CircularProgressIndicator());
  static Widget initial() => Center(
      key: weatherSummaryInitialKey,
      child: Text('Please enter a location to get started.'));
  static Widget error(dynamic err, StackTrace stacktrace) => Center(
      key: weatherSummaryErrorKey,
      child: Text('Issue with that location, please try again.'));

  @visibleForTesting
  static const weatherSummaryPlaceholderKey =
      Key('weather_summary_placeholder');
  @visibleForTesting
  static const weatherSummaryInitialKey = Key('weather_summary_initial');
  @visibleForTesting
  static const weatherSummaryErrorKey = Key('weather_summary_error');
}
