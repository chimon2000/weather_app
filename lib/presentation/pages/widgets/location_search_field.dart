import 'package:flutter/material.dart';

class LocationSearchFormField extends StatelessWidget {
  const LocationSearchFormField({
    Key key,
    this.initialValue,
    this.onChanged,
    this.validator,
    this.onSearchTap,
    this.onFieldSubmitted,
  }) : super(key: key);

  final ValueChanged onChanged;
  final FormFieldValidator<String> validator;
  final VoidCallback onSearchTap;
  final String initialValue;
  final ValueChanged<String> onFieldSubmitted;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: locationSearchFormFieldKey,
      initialValue: initialValue,
      onChanged: onChanged,
      onFieldSubmitted: onFieldSubmitted,
      validator: validator,
      textInputAction: TextInputAction.search,
      decoration: InputDecoration(
        hintText: 'Enter the city name',
        border: OutlineInputBorder(),
        suffixIcon: GestureDetector(
          onTap: onSearchTap,
          child: Icon(Icons.search, size: 32, key: locationSearchIconKey),
        ),
      ),
    );
  }

  @visibleForTesting
  static const locationSearchFormFieldKey =
      Key('location_search_form_field_key');
  @visibleForTesting
  static const locationSearchIconKey = Key('location_search_icon_key');
}
