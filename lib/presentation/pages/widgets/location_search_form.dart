import 'package:flutter/material.dart';
import 'package:weather_app/presentation/logic/home_page.logic.dart';
import 'package:weather_app/presentation/pages/widgets/location_search_field.dart';
import 'package:binder/binder.dart';
import 'package:weather_app/presentation/validators/inputs/required_input.dart';

class LocationSearchForm extends StatelessWidget {
  LocationSearchForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      key: locationSearchFormKey,
      child: Column(
        children: [
          LocationSearchFormField(
            initialValue: context
                .watch(homePageStateRef.select((state) => state.location))
                ?.title,
            onChanged: (value) => _handleChanged(context, value),
            validator: (value) => _handleValidator(context, value),
            onSearchTap: () => _handleSearch(context),
            onFieldSubmitted: (_) => _handleSearch(context),
          ),
        ],
      ),
    );
  }

  void _handleChanged(BuildContext context, String value) {
    final validator = context
        .read(homePageValidatorRef)
        .copyWith(locationSearchQuery: RequiredInput.dirty(value: value));

    context.use(homePageLogicRef).updateValidator(validator);
  }

  String _handleValidator(BuildContext context, String value) {
    final validator = context
        .read(homePageValidatorRef)
        .copyWith(locationSearchQuery: RequiredInput.dirty(value: value));

    return validator?.locationSearchQuery?.error == RequiredInputError.empty ??
            false
        ? 'City name is required'
        : null;
  }

  void _handleSearch(BuildContext context) {
    final isValid = locationSearchFormKey.currentState.validate();
    if (isValid) {
      FocusScope.of(context).requestFocus(FocusNode());

      context.use(homePageLogicRef).searchLocation();
    }
  }

  @visibleForTesting
  static final locationSearchFormKey = GlobalKey<FormState>();
}
