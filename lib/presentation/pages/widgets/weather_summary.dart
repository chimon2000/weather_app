import 'package:flutter/material.dart';
import 'package:weather_app/data/data.dart';
import 'package:weather_app/presentation/extensions/extensions.dart';
import 'package:weather_app/presentation/logic/states/home_page.state.dart';

class WeatherSummary extends StatelessWidget {
  const WeatherSummary(
      {Key key, @required this.weather, @required this.tempuratureUnit})
      : super(key: key);

  final Weather weather;
  final TempuratureUnit tempuratureUnit;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              weather.tempDisplay(tempuratureUnit),
              style: textTheme.headline2,
            ),
          ],
        ),
        Text.rich(
          TextSpan(
            text: '${weather.stateDisplay} ',
            children: [
              TextSpan(text: weather.minMaxTempDisplay(tempuratureUnit))
            ],
          ),
          style: textTheme.subtitle1,
        ),
        SizedBox(height: 24),
        Text('Detail', style: textTheme.headline5),
        _buildDetailRow(children: [
          _buildDetailColumn(
              title: 'Wind Direction', detail: weather.windDirection),
          _buildDetailColumn(
              title: 'Wind Speed', detail: weather.windSpeedDisplay),
        ]),
        SizedBox(height: 8),
        _buildDetailRow(
          children: [
            _buildDetailColumn(
                title: 'Pressure', detail: weather.airPressureDisplay),
            _buildDetailColumn(
                title: 'Visibility', detail: weather.visibilityDisplay),
          ],
        ),
        SizedBox(height: 8),
        _buildDetailRow(
          children: [
            _buildDetailColumn(
                title: 'Humidity', detail: weather.humidityDisplay),
          ],
        )
      ],
    );
  }

  Widget _buildDetailRow({List<Widget> children}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: children.map((child) => Expanded(child: child)).toList(),
    );
  }

  Widget _buildDetailColumn({String title, String detail}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(title),
        Text(detail),
      ],
    );
  }
}
