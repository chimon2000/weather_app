import 'package:flutter/material.dart';
import 'package:weather_app/presentation/logic/home_page.logic.dart';
import 'package:weather_app/presentation/logic/states/home_page.state.dart';
import 'package:weather_app/presentation/pages/widgets/widgets.dart';
import 'package:binder/binder.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key, @required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: [_buildSettingsMenu()],
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: SafeArea(
        child: RefreshIndicator(
          key: homeRefreshIndicatorKey,
          onRefresh: () => context.use(homePageLogicRef).getWeather(),
          child: SingleChildScrollView(
            key: homeScrollViewKey,
            physics: AlwaysScrollableScrollPhysics(),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  SizedBox(height: 8),
                  LocationSearchForm(),
                  SizedBox(height: 32),
                  WeatherSummaryContainer(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSettingsMenu() => Consumer(
        watchable: homePageStateRef.select((state) => state.tempuratureUnit),
        builder: (context, tempuratureUnit, child) => PopupMenuButton(
          key: homeSettingsButtonKey,
          itemBuilder: (context) => [
            PopupMenuItem(
              key: homeTempuratureToggleButtonKey,
              child: Text(tempuratureUnit == TempuratureUnit.celcius
                  ? 'Show Farenheit'
                  : 'Show Celcius'),
              value: 1,
            )
          ],
          onSelected: (value) {
            if (value == 1) {
              context.use(homePageLogicRef).toggleTempuratureUnit();
            }
          },
        ),
      );

  @visibleForTesting
  static const homeRefreshIndicatorKey = Key('home_refresh_indicator');
  @visibleForTesting
  static const homeScrollViewKey = Key('home_scroll_view');
  @visibleForTesting
  static const homeSettingsButtonKey = Key('home_settings_button_key');
  @visibleForTesting
  static const homeTempuratureToggleButtonKey =
      Key('home_tempurature_toggle_button_key');
}
