import 'package:weather_app/data/data.dart';
import 'package:charcode/charcode.dart';
import 'package:weather_app/presentation/logic/states/home_page.state.dart';

extension DisplayEnhancements on Weather {
  String tempDisplay(TempuratureUnit tempuratureUnit) {
    return tempuratureUnit == TempuratureUnit.celcius
        ? '${theTemp.toInt()}${String.fromCharCode($deg)}C'
        : '${toFarenheit(theTemp).toInt()}${String.fromCharCode($deg)}F';
  }

  String minMaxTempDisplay(TempuratureUnit tempuratureUnit) {
    return tempuratureUnit == TempuratureUnit.celcius
        ? '${maxTemp.toInt()} / ${minTemp.toInt()}${String.fromCharCode($deg)}C'
        : '${toFarenheit(maxTemp).toInt()} / ${toFarenheit(minTemp).toInt()}${String.fromCharCode($deg)}F';
  }

  String get stateDisplay {
    return stateDisplayMap[weatherStateAbbr];
  }

  String get humidityDisplay => '${humidity.toInt()}%';

  String get windSpeedDisplay => '${windSpeed.toInt()} mph';

  String get visibilityDisplay => '${visibility.toInt()} miles';

  String get airPressureDisplay => '${airPressure.toStringAsFixed(2)} mb';
}

const stateDisplayMap = {
  'c': 'Clear',
  'h': 'Hail',
  'hc': 'Heavy clouds',
  'hr': 'Heavy rain',
  'lc': 'Light clouds',
  'lr': 'Light rain',
  's': 'Snow',
  'sn': 'Snow',
  'sl': 'Sleet',
  't': 'Thunderstorms',
};

double toFarenheit(double celcius) => (celcius * 1.8) + 32;
