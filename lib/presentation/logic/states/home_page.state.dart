import 'package:equatable/equatable.dart';
import 'package:remote_state/remote_state.dart';
import 'package:weather_app/data/data.dart';

class HomePageState extends Equatable {
  HomePageState._({
    this.weather,
    this.location,
    this.tempuratureUnit,
  });

  HomePageState.initial()
      : weather = RemoteState.initial(),
        location = null,
        tempuratureUnit = TempuratureUnit.celcius;

  final RemoteState<Weather> weather;
  final Location location;
  final TempuratureUnit tempuratureUnit;

  @override
  List<Object> get props => [weather, location];

  HomePageState copyWith({
    RemoteState<Weather> weather,
    Location location,
    TempuratureUnit tempuratureUnit,
  }) {
    return HomePageState._(
      weather: weather ?? this.weather,
      location: location ?? this.location,
      tempuratureUnit: tempuratureUnit ?? this.tempuratureUnit,
    );
  }
}

enum TempuratureUnit {
  celcius,
  farenheit,
}
