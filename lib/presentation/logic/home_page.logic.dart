import 'package:binder/binder.dart';
import 'package:remote_state/remote_state.dart';
import 'package:weather_app/business/repositories/location.repository.dart';
import 'package:weather_app/business/repositories/weather.repository.dart';

import 'package:weather_app/presentation/logic/states/states.dart';
import 'package:weather_app/presentation/validators/validators.dart';

final homePageStateRef = StateRef(HomePageState.initial());
final homePageValidatorRef = StateRef(HomePageValidator());
final homePageLogicRef = LogicRef((scope) => HomePageLogic(scope));

class HomePageLogic with Logic {
  const HomePageLogic(this.scope);

  @override
  final Scope scope;

  Future<void> searchLocation() async {
    final query = read(homePageValidatorRef).locationSearchQuery.value;
    if (query?.isNotEmpty ?? false) {
      final location = await _locationRepository.searchLocation(query);
      final updatedState = read(homePageStateRef).copyWith(location: location);

      write(homePageStateRef, updatedState);
    }
  }

  Future<void> getWeather() async {
    final locationId = read(homePageStateRef)?.location?.id;

    if (locationId != null) {
      if (read(homePageStateRef).weather.isInitial) {
        write(homePageStateRef,
            read(homePageStateRef).copyWith(weather: RemoteState.loading()));
      }

      final weather = await RemoteState.guard(
          () => _weatherRepository.getWeather(locationId));
      final updatedState = read(homePageStateRef).copyWith(weather: weather);

      write(homePageStateRef, updatedState);
    }
  }

  void updateValidator(HomePageValidator validator) {
    write(homePageValidatorRef, validator);
  }

  void toggleTempuratureUnit() {
    final homePageState = read(homePageStateRef);
    final tempuratureUnit =
        homePageState.tempuratureUnit == TempuratureUnit.celcius
            ? TempuratureUnit.farenheit
            : TempuratureUnit.celcius;
    write(homePageStateRef,
        homePageState.copyWith(tempuratureUnit: tempuratureUnit));
  }

  LocationRepository get _locationRepository => use(locationRepositoryRef);
  WeatherRepository get _weatherRepository => use(weatherRepositoryRef);
}
