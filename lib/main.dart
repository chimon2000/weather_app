import 'package:binder/binder.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:weather_app/app.dart';
import 'package:weather_app/data/data.dart';

// coverage:ignore-start
void main() {
  final dio = Dio(BaseOptions(baseUrl: 'https://www.metaweather.com/api'));

  runApp(
    BinderScope(
      overrides: [
        metaWeatherDataSourceRef.overrideWith(MetaWeatherDataSource(dio)),
      ],
      child: WeatherApp(),
    ),
  );
}
// coverage:ignore-end
