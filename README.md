# Weather App

A Flutter App demonstrating the use of the MetaWeather API

## Getting Started

Run `flutter run` in your project root. If you see any issues, please start from step 1 below.

### Step 1 - Install Flutter dependencies

In order to started on development you will need to be sure you have the needed tools for Flutter and Dart development. You can start with the [Google provided guide](https://flutter.dev/docs/get-started/install) for your platform.

### Step 2 - Install project dependencies

Run `flutter pub get` in your project root to install the project dependencies.

## Architecture

You can learn more about the architecture of this application [here](./docs/architecture.md).
