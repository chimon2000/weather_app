# Weather App

Weather App Flutter application.

## Architecture

This is a very high-level summary of each layer of the application.

### Common

Common is responsible for common utility functions and extensions used across the application.

### Business

Business is responsible for application specific logic that encapsulate the logic required to access data sources.

### Data

Data is responsible for

| Folder  | Description                       |
| ------- | :-------------------------------- |
| sources | Data sources for data acquisition |
| models  | Domain models                     |

### Presentation

Presentation includes all presentation components for the application.

| Folder     | Description                                                 |
| ---------- | :---------------------------------------------------------- |
| extensions | Presentation specific computed properties, extensions, etc. |
| logic      | Page specific logic                                         |
| pages      | Page level widgets                                          |
