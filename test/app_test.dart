// ignore_for_file: unused_import
import 'package:binder/binder.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:weather_app/app.dart';
import 'package:weather_app/main.dart';

void main() {
  group('WeatherApp', () {
    testWidgets('Smoke screen test', (WidgetTester tester) async {
      await tester.pumpWidget(BinderScope(child: WeatherApp()));
    });
  });
}
