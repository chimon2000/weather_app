import 'package:binder/binder.dart';
import 'package:dio/dio.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:weather_app/business/repositories/location.repository.dart';
import 'package:weather_app/data/data.dart';

void main() {
  group('LocationRepository', () {
    Scope scope;
    LocationRepository locationRepository;
    MetaWeatherDataSource metaWeatherDataSource;

    setUp(() {
      metaWeatherDataSource = MockMetaWeatherDataSource();
      scope = MockScope();
      locationRepository = LocationRepository(scope);
    });

    test('searchLocation', () async {
      when(scope.read(metaWeatherDataSourceRef))
          .thenReturn(metaWeatherDataSource);
      when(metaWeatherDataSource.searchLocation(any))
          .thenAnswer((_) async => Location.fromMap(_locationMap));

      expect(await locationRepository.searchLocation(''),
          Location.fromMap(_locationMap));
    });

    test('searchLocation throws on error', () async {
      when(scope.read(metaWeatherDataSourceRef))
          .thenReturn(metaWeatherDataSource);
      when(metaWeatherDataSource.searchLocation(any))
          .thenAnswer((_) async => throw LocationSearchRequestFailure());

      expect(
        () async => await locationRepository.searchLocation(''),
        throwsA(isInstanceOf<LocationSearchRequestFailure>()),
      );
    });
  });
}

class MockDioClient extends Mock implements Dio {}

class MockScope extends Mock implements Scope {}

class MockMetaWeatherDataSource extends Mock implements MetaWeatherDataSource {}

const _locationMap = {
  'title': 'Charlotte',
  'location_type': 'City',
  'woeid': 2378426,
  'latt_long': '35.222500,-80.837540'
};
