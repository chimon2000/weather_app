import 'package:binder/binder.dart';
import 'package:dio/dio.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:weather_app/business/business.dart';
import 'package:weather_app/data/data.dart';

void main() {
  group('WeatherRepository', () {
    Scope scope;
    WeatherRepository locationRepository;
    MetaWeatherDataSource metaWeatherDataSource;

    setUp(() {
      metaWeatherDataSource = MockMetaWeatherDataSource();
      scope = MockScope();
      locationRepository = WeatherRepository(scope);
    });

    test('getWeather', () async {
      when(scope.read(metaWeatherDataSourceRef))
          .thenReturn(metaWeatherDataSource);
      when(metaWeatherDataSource.getWeather(any))
          .thenAnswer((_) async => Weather.fromMap(_weatherMap));

      expect(
          await locationRepository.getWeather(0), Weather.fromMap(_weatherMap));
    });

    test('getWeather throws on error', () async {
      when(scope.read(metaWeatherDataSourceRef))
          .thenReturn(metaWeatherDataSource);
      when(metaWeatherDataSource.getWeather(any))
          .thenAnswer((_) async => throw WeatherRequestFailure());

      expect(
        () async => await locationRepository.getWeather(0),
        throwsA(isInstanceOf<WeatherRequestFailure>()),
      );
    });
  });
}

class MockDioClient extends Mock implements Dio {}

class MockScope extends Mock implements Scope {}

class MockMetaWeatherDataSource extends Mock implements MetaWeatherDataSource {}

// ignore_for_file: prefer_single_quotes
const _weatherMap = {
  "id": 6565250670264320,
  "weather_state_name": "Heavy Cloud",
  "weather_state_abbr": "hc",
  "wind_direction_compass": "SW",
  "created": "2020-12-14T00:51:19.828363Z",
  "applicable_date": "2020-12-13",
  "min_temp": 11.93,
  "max_temp": 20.33,
  "the_temp": 18.455,
  "wind_speed": 3.8502720352130226,
  "wind_direction": 231.94806165629424,
  "air_pressure": 1018.5,
  "humidity": 75,
  "visibility": 10.972793883719081,
  "predictability": 71
};
