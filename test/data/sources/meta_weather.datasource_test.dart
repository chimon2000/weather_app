import 'package:dio/dio.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:weather_app/data/data.dart';

void main() {
  group('MetaWeatherDataSource', () {
    MetaWeatherDataSource metaWeatherDataSource;
    Dio dio;
    MockResponse response;

    setUp(() {
      dio = MockDioClient();
      metaWeatherDataSource = MetaWeatherDataSource(dio);
      response = MockResponse();
    });

    test('searchLocation', () async {
      when(response.statusCode).thenReturn(200);
      when(response.data).thenReturn([]);
      when(dio.get(any, queryParameters: anyNamed('queryParameters')))
          .thenAnswer((realInvocation) async => response);

      expect(await metaWeatherDataSource.searchLocation(''), null);

      when(response.statusCode).thenReturn(200);
      when(response.data).thenReturn([_locationMap]);

      expect(await metaWeatherDataSource.searchLocation(''),
          Location.fromMap(_locationMap));
    });

    test('searchLocation throws', () async {
      when(response.statusCode).thenReturn(400);
      when(dio.get(any, queryParameters: anyNamed('queryParameters')))
          .thenAnswer((realInvocation) async => response);

      expect(
        () async => await metaWeatherDataSource.searchLocation(''),
        throwsA(isInstanceOf<LocationSearchRequestFailure>()),
      );
    });

    test('getWeather', () async {
      when(response.statusCode).thenReturn(200);
      when(response.data).thenReturn({});
      when(dio.get(any)).thenAnswer((realInvocation) async => response);

      expect(await metaWeatherDataSource.getWeather(0), null);

      when(response.statusCode).thenReturn(200);
      when(response.data).thenReturn(_weatherResponseMap);

      expect(await metaWeatherDataSource.getWeather(0),
          Weather.fromMap(_weatherMap));
    });

    test('getWeather throws', () async {
      when(response.statusCode).thenReturn(400);
      when(dio.get(any)).thenAnswer((realInvocation) async => response);

      expect(
        () async => await metaWeatherDataSource.getWeather(0),
        throwsA(isInstanceOf<WeatherRequestFailure>()),
      );
    });
  });
}

class MockDioClient extends Mock implements Dio {}

class MockResponse extends Mock implements Response {}

// ignore_for_file: prefer_single_quotes
const _locationMap = {
  'title': 'Charlotte',
  'location_type': 'City',
  'woeid': 2378426,
  'latt_long': '35.222500,-80.837540'
};

const _weatherResponseMap = {
  "consolidated_weather": [_weatherMap]
};

const _weatherMap = {
  "id": 6565250670264320,
  "weather_state_name": "Heavy Cloud",
  "weather_state_abbr": "hc",
  "wind_direction_compass": "SW",
  "created": "2020-12-14T00:51:19.828363Z",
  "applicable_date": "2020-12-13",
  "min_temp": 11.93,
  "max_temp": 20.33,
  "the_temp": 18.455,
  "wind_speed": 3.8502720352130226,
  "wind_direction": 231.94806165629424,
  "air_pressure": 1018.5,
  "humidity": 75,
  "visibility": 10.972793883719081,
  "predictability": 71
};
