import 'package:binder/binder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:weather_app/data/data.dart';
import 'package:weather_app/presentation/logic/home_page.logic.dart';
import 'package:weather_app/presentation/logic/states/states.dart';
import 'package:weather_app/presentation/pages/pages.dart';

import '../../util/wrappers.dart';

void main() {
  group('HomePage', () {
    MetaWeatherDataSource metaWeatherDataSource;
    HomePageLogic homePageLogic;
    List<BinderOverride<dynamic>> overrides;

    setUp(() {
      metaWeatherDataSource = MockMetaWeatherDataSource();
      homePageLogic = MockHomePageLogic();
      overrides = [
        metaWeatherDataSourceRef.overrideWith(metaWeatherDataSource),
        homePageLogicRef.overrideWith((scope) => homePageLogic),
        homePageStateRef.overrideWith(HomePageState.initial()),
      ];
    });

    testWidgets('smoke test', (WidgetTester tester) async {
      when(homePageLogic.getWeather()).thenAnswer((realInvocation) async => {});

      await tester.pumpWidget(
          appWrapper(HomePage(title: 'Weather app'), overrides: overrides));

      expect(find.text('Weather app'), findsWidgets);
      expect(find.byKey(HomePage.homeRefreshIndicatorKey), findsWidgets);

      await tester.fling(find.byKey(HomePage.homeScrollViewKey),
          const Offset(0.0, 300.0), 1000.0);

      await tester.pump();
      await tester.pump(Duration(seconds: 1));
      await tester.pump(Duration(seconds: 1));
      await tester.pump(Duration(seconds: 1));

      verify(homePageLogic.getWeather()).called(1);
    });

    testWidgets('toggles tempurature unit', (WidgetTester tester) async {
      await tester.pumpWidget(appWrapper(HomePage(title: 'Weather app')));

      expect(find.text('Weather app'), findsWidgets);

      await tester.tap(find.byKey(HomePage.homeSettingsButtonKey));

      await tester.pumpAndSettle();

      expect(find.text('Show Farenheit'), findsOneWidget);
      expect(
          find.byKey(HomePage.homeTempuratureToggleButtonKey), findsOneWidget);

      await tester.tap(find.byKey(HomePage.homeTempuratureToggleButtonKey));

      await tester.pumpAndSettle();

      await tester.tap(find.byKey(HomePage.homeSettingsButtonKey));

      await tester.pumpAndSettle();

      expect(find.text('Show Celcius'), findsWidgets);
    });
  });
}

class MockMetaWeatherDataSource extends Mock implements MetaWeatherDataSource {}

class MockHomePageLogic extends Mock implements HomePageLogic {}
