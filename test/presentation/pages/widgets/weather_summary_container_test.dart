import 'package:binder/binder.dart';
import 'package:binder/src/build_context_extensions.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:remote_state/remote_state.dart';
import 'package:weather_app/data/data.dart';
import 'package:weather_app/presentation/logic/home_page.logic.dart';
import 'package:weather_app/presentation/logic/states/states.dart';
import 'package:weather_app/presentation/pages/widgets/weather_summary.dart';
import 'package:weather_app/presentation/pages/widgets/widgets.dart';

import '../../../util/wrappers.dart';

void main() {
  group('WeatherSummaryContainer', () {
    MetaWeatherDataSource metaWeatherDataSource;
    HomePageLogic homePageLogic;
    List<BinderOverride<dynamic>> overrides;

    setUp(() {
      metaWeatherDataSource = MockMetaWeatherDataSource();
      homePageLogic = MockHomePageLogic();
      overrides = [
        metaWeatherDataSourceRef.overrideWith(metaWeatherDataSource),
        homePageLogicRef.overrideWith((scope) => homePageLogic),
        homePageStateRef.overrideWith(HomePageState.initial()),
      ];
    });

    testWidgets('WeatherSummaryContainer smoke test',
        (WidgetTester tester) async {
      BuildContext c0;

      await tester.pumpWidget(scaffoldWrapper(
        Builder(builder: (context) {
          c0 = context;

          return WeatherSummaryContainer();
        }),
        overrides: overrides,
      ));

      expect(find.byKey(WeatherSummaryContainer.weatherSummaryInitialKey),
          findsWidgets);

      c0.write(
          homePageStateRef,
          HomePageState.initial()
              .copyWith(location: Location.fromMap(_locationMap)));

      await tester.pump();

      verify(homePageLogic.getWeather()).called(1);
    });

    testWidgets('WeatherSummaryContainer.initial', (WidgetTester tester) async {
      await tester.pumpWidget(scaffoldWrapper(
        WeatherSummaryContainer(),
        overrides: overrides,
      ));

      expect(find.byKey(WeatherSummaryContainer.weatherSummaryInitialKey),
          findsWidgets);
    });

    testWidgets('WeatherSummaryContainer.success', (WidgetTester tester) async {
      BuildContext c0;
      when(metaWeatherDataSource.searchLocation(any))
          .thenAnswer((_) async => Location.fromMap(_locationMap));
      when(metaWeatherDataSource.getWeather(any))
          .thenAnswer((_) async => Weather.fromMap(_weatherMap));

      overrides = [
        metaWeatherDataSourceRef.overrideWith(metaWeatherDataSource),
        homePageStateRef.overrideWith(HomePageState.initial()),
      ];

      await tester.pumpWidget(scaffoldWrapper(
        Builder(builder: (context) {
          c0 = context;
          return WeatherSummaryContainer();
        }),
        overrides: overrides,
      ));

      expect(find.byKey(WeatherSummaryContainer.weatherSummaryInitialKey),
          findsWidgets);

      c0.write(
          homePageStateRef,
          HomePageState.initial()
              .copyWith(location: Location.fromMap(_locationMap)));

      await tester.pump();
      await tester.pump();

      expect(find.byType(WeatherSummary), findsWidgets);
    });

    testWidgets('WeatherSummaryContainer.error', (WidgetTester tester) async {
      overrides = [
        metaWeatherDataSourceRef.overrideWith(metaWeatherDataSource),
        homePageLogicRef.overrideWith((scope) => homePageLogic),
        homePageStateRef.overrideWith(
            HomePageState.initial().copyWith(weather: RemoteState.error())),
      ];
      await tester.pumpWidget(scaffoldWrapper(
        WeatherSummaryContainer(),
        overrides: overrides,
      ));

      expect(find.byKey(WeatherSummaryContainer.weatherSummaryErrorKey),
          findsWidgets);
    });

    testWidgets('WeatherSummaryContainer.placeholder',
        (WidgetTester tester) async {
      overrides = [
        metaWeatherDataSourceRef.overrideWith(metaWeatherDataSource),
        homePageLogicRef.overrideWith((scope) => homePageLogic),
        homePageStateRef.overrideWith(
            HomePageState.initial().copyWith(weather: RemoteState.loading())),
      ];
      await tester.pumpWidget(scaffoldWrapper(
        WeatherSummaryContainer(),
        overrides: overrides,
      ));

      expect(find.byKey(WeatherSummaryContainer.weatherSummaryPlaceholderKey),
          findsWidgets);
    });
  });
}

class MockMetaWeatherDataSource extends Mock implements MetaWeatherDataSource {}

class MockHomePageLogic extends Mock implements HomePageLogic {}

// ignore_for_file: prefer_single_quotes
const _locationMap = {
  'title': 'Charlotte',
  'location_type': 'City',
  'woeid': 2378426,
  'latt_long': '35.222500,-80.837540'
};
const _weatherMap = {
  "id": 6565250670264320,
  "weather_state_name": "Heavy Cloud",
  "weather_state_abbr": "hc",
  "wind_direction_compass": "SW",
  "created": "2020-12-14T00:51:19.828363Z",
  "applicable_date": "2020-12-13",
  "min_temp": 11.93,
  "max_temp": 20.33,
  "the_temp": 18.455,
  "wind_speed": 3.8502720352130226,
  "wind_direction": 231.94806165629424,
  "air_pressure": 1018.5,
  "humidity": 75,
  "visibility": 10.972793883719081,
  "predictability": 71
};
