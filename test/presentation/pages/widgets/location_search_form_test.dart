import 'package:binder/binder.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:weather_app/business/repositories/location.repository.dart';
import 'package:weather_app/data/data.dart';
import 'package:weather_app/presentation/logic/home_page.logic.dart';
import 'package:weather_app/presentation/logic/states/states.dart';
import 'package:weather_app/presentation/pages/widgets/location_search_field.dart';
import 'package:weather_app/presentation/pages/widgets/widgets.dart';

import '../../../util/wrappers.dart';

void main() {
  group('LocationSearchForm', () {
    MetaWeatherDataSource metaWeatherDataSource;
    List<BinderOverride<dynamic>> overrides;
    LocationRepository locationRepository;

    setUp(() {
      metaWeatherDataSource = MockMetaWeatherDataSource();
      locationRepository = MockLocationRepository();
      overrides = [
        metaWeatherDataSourceRef.overrideWith(metaWeatherDataSource),
        locationRepositoryRef.overrideWith((scope) => locationRepository),
        homePageStateRef.overrideWith(HomePageState.initial()),
      ];
    });

    testWidgets('LocationSearchForm smoke test', (WidgetTester tester) async {
      when(locationRepository.searchLocation(any))
          .thenAnswer((realInvocation) async => Location.fromMap(_locationMap));
      await tester.pumpWidget(scaffoldWrapper(
        LocationSearchForm(),
        overrides: overrides,
      ));

      await tester
          .tap(find.byKey(LocationSearchFormField.locationSearchIconKey));

      await tester.pump();

      expect(find.text('City name is required'), findsOneWidget);

      await tester.enterText(
          find.byKey(LocationSearchFormField.locationSearchFormFieldKey),
          'charlotte');
      await tester.testTextInput.receiveAction(TextInputAction.done);

      await tester.pump();

      expect(find.text('City name is required'), findsNothing);
    });

    testWidgets('LocationSearchForm changes location',
        (WidgetTester tester) async {
      Location location;
      when(locationRepository.searchLocation(any))
          .thenAnswer((realInvocation) async => Location.fromMap(_locationMap));

      await tester.pumpWidget(scaffoldWrapper(
        StateListener(
          onStateChanged: (context, state) {
            location = state;
          },
          watchable: homePageStateRef.select((state) => state.location),
          child: LocationSearchForm(),
        ),
        overrides: overrides,
      ));

      await tester.enterText(
          find.byKey(LocationSearchFormField.locationSearchFormFieldKey),
          'charlotte');

      await tester.pump();

      expect(find.text('City name is required'), findsNothing);

      await tester.enterText(
          find.byKey(LocationSearchFormField.locationSearchFormFieldKey),
          'charlotte');

      await tester
          .tap(find.byKey(LocationSearchFormField.locationSearchIconKey));

      await tester.pump();

      expect(location, Location.fromMap(_locationMap));
    });
  });
}

class MockMetaWeatherDataSource extends Mock implements MetaWeatherDataSource {}

class MockHomePageLogic extends Mock implements HomePageLogic {}

class MockLocationRepository extends Mock implements LocationRepository {}

// ignore_for_file: prefer_single_quotes
const _locationMap = {
  'title': 'Charlotte',
  'location_type': 'City',
  'woeid': 2378426,
  'latt_long': '35.222500,-80.837540'
};
