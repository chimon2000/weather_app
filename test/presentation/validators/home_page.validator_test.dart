import 'package:flutter_test/flutter_test.dart';
import 'package:formz/formz.dart';
import 'package:weather_app/presentation/validators/home_page.validator.dart';
import 'package:weather_app/presentation/validators/inputs/inputs.dart';

void main() {
  group('HomePageValidator', () {
    test('description', () {
      var validator = HomePageValidator();

      expect(validator, validator.copyWith());
      expect(validator.locationSearchQuery, RequiredInput.pure());
      expect(validator.status, FormzStatus.pure);

      validator = validator.copyWith(
          locationSearchQuery: RequiredInput.dirty(value: ''));
      expect(validator.status, FormzStatus.invalid);

      validator = validator.copyWith(
          locationSearchQuery: RequiredInput.dirty(value: 'Charlotte'));
      expect(validator.status, FormzStatus.valid);
    });
  });
}
