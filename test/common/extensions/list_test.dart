import 'package:flutter_test/flutter_test.dart';
import 'package:weather_app/common/common.dart';

void main() {
  group('IsNullOrEmptyEnhancements', () {
    test('isNullOrEmpty', () async {
      List list;

      expect(list.isNullOrEmpty, true);

      list = [];

      expect(list.isNullOrEmpty, true);

      list = [0];

      expect(list.isNullOrEmpty, false);
    });
  });
}
