import 'package:binder/binder.dart';
import 'package:flutter/material.dart';

Widget scaffoldWrapper(
  Widget child, {
  List<BinderOverride<dynamic>> overrides,
  List<StateObserver> observers = const [],
}) =>
    BinderScope(
        overrides: overrides,
        observers: observers,
        child: MaterialApp(home: Scaffold(body: child)));

Widget appWrapper(
  Widget child, {
  List<BinderOverride<dynamic>> overrides = const [],
  List<StateObserver> observers = const [],
}) =>
    BinderScope(
        overrides: overrides,
        observers: observers,
        child: MaterialApp(home: child));
